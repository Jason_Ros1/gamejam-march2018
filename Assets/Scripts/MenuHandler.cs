﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuHandler : MonoBehaviour {

	bool gameStopped;
	bool inCredits;

	[Header("Game Manager")]
	[SerializeField] private GameObject gameManager;
	/**************/ private GameStatManager gameStatManager;
    /**************/ private MusicManager musicManager;

	[Header("UI")]
	[SerializeField] private GameObject mainMenu;
	[SerializeField] private GameObject HUD;
	[SerializeField] private GameObject pauseScreen;
	[SerializeField] private GameObject credits;

	[Header("Individual Objects")]
	[SerializeField] private Image healthBar;

	GameObject o;

	// Use this for initialization
	void Start () {
		this.gameStatManager = gameManager.GetComponent<GameStatManager> ();
        this.musicManager = gameManager.GetComponent<MusicManager>();

		pauseScreen.SetActive (true);
		pauseScreen.SetActive (false);
		credits.SetActive (true);
		credits.SetActive (false);

		gameStopped = true;
		Time.timeScale = 0;

		creditsToggle (false);

        musicManager.playMenu();
	}
	
	// Update is called once per frame
	void Update () {
		if (!gameStopped) {
			updateHealthAggregate ();
			if(Input.GetKeyUp(KeyCode.P)){
				pauseGame (true);
			}
		}
	}

	public void onClickPlay(){
		mainMenu.SetActive (false);
		pauseGame (false);
        musicManager.playGameMusic();
	}

	public void onClickCredits(){
		creditsToggle (true);
	}

	public void onClickCreditsBack(){
		creditsToggle (false);
	}

	public void onClickResume(){
		pauseGame(false);
	}

	public void quitGame(){
		Application.Quit ();
	}

	private void pauseGame(bool pause){
		if (pause) {
			pauseScreen.SetActive (true);
			HUD.SetActive (false);
			Time.timeScale = 0;
		} else {
			pauseScreen.SetActive (false);
			HUD.SetActive (true);
			Time.timeScale = 1;
		}

		gameStopped = pause;
	}

	public void backtoMainMenu(){
		Scene s = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (s.name);
	}

	private void creditsToggle(bool cred){
		if (cred) {
			mainMenu.SetActive (false);
			credits.SetActive (true);
		} else {
			credits.SetActive (false);
			mainMenu.SetActive (true);
		}

		inCredits = cred;
	}

	private void updateHealthAggregate(){
		float percentage = this.gameStatManager.getBattleStrPercentage();
		this.healthBar.fillAmount = percentage;
	}



}
