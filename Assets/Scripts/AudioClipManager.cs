﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioClipManager : MonoBehaviour {

    //List<AudioClip> MusicList = new List<AudioClip>();
    public List<AudioClip> SwordClip = new List<AudioClip>();
    public List<AudioClip> DeathClip = new List<AudioClip>();

    AudioSource ads;

    // Use this for initialization
    private void OnEnable()
    {
        ads = this.gameObject.GetComponent<AudioSource>();
    }

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void playSword()
    {
        if(SwordClip.Count > 0)
        {
            //Debug.Log(SwordClip.Count);
            AudioClip tmp = SwordClip[UnityEngine.Random.Range(0,SwordClip.Count - 1)];
            ads.PlayOneShot(tmp);
        }
    }

    public void playDeath()
    {
        if (DeathClip.Count > 0)
        {
            //Debug.Log(SwordClip.Count);
            AudioClip tmp = DeathClip[UnityEngine.Random.Range(0, SwordClip.Count - 1)];
            ads.PlayOneShot(tmp);
        }
    }
}
