﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class Agression : MonoBehaviour {
	bool isChasing;
    SphereCollider sc;

    [SerializeField] private float aggroRadius = 2f;
    // Use this for initialization
    [SerializeField] public List<GameObject> gO = new List<GameObject>();
    public string EnemyTag;
    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.yellow;
    //    Gizmos.DrawWireSphere(this.transform.position, aggroRadius);
    //}

    private void OnEnable()
    {
        sc = this.gameObject.GetComponent<SphereCollider>();
        //sc.center = (new Vector3(0, 0.5f, 0) + this.gameObject.transform.parent.position);
        sc.radius = aggroRadius;
        sc.isTrigger = true;
		isChasing = false;
    }

    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        //if (other.transform.parent.tag == EnemyTag)
        //{
        //    gO.Add(other.gameObject);
        //}
        //Debug.Log(other.gameObject);
        //Debug.Log("Triggered");
        if(other.gameObject.tag == EnemyTag)
        {
            gO.Add(other.gameObject);
			if (gO.Count > 0)
				isChasing = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == EnemyTag)
        {
            gO.Remove(other.gameObject);
			if (gO.Count <= 0)
				isChasing = false;
			if(other.gameObject.tag == "White"){
				other.gameObject.GetComponent<AI> ().targetSet (other.gameObject.transform.position);
			}
        }
    }

    public GameObject getClosest()
    {
        GameObject _closestObj = null;
        float closestFloat = 0;

        List<GameObject> toBeRemoved = new List<GameObject>();

        foreach(GameObject i in gO)
        {
            if(i == null)
            {
                toBeRemoved.Add(i);
                continue;
            }
            float tmp = (this.transform.position - i.gameObject.transform.position).magnitude;
            if(_closestObj == null || tmp < closestFloat)
            {
                closestFloat = tmp;
                _closestObj = i;
            }
        }

        foreach (GameObject i in toBeRemoved)
        {
            gO.Remove(i);
        }

        return _closestObj;
    }

	public bool isChasingEnemy(){
		return isChasing;
	}

    public void removeFromList(GameObject item)
    {
        gO.Remove(item);
    }
}
