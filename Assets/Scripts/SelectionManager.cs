﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : MonoBehaviour {

	FormationManager formationManager;
	[SerializeField] List<GameObject> selected;

	// Use this for initialization
	void Start () {
		formationManager = this.gameObject.GetComponent<FormationManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/************************
	 *	PUBLIC FUNCTIONS	*
	 ************************/
	public void selectFromBox(Vector3 clickDown, Vector3 clickUp){
		Debug.Log ("Asking to select from box of " + clickDown + " " + clickUp );
		selected = new List<GameObject> ();
		Debug.Log ("List length " + selected.Count);

		for (int i = 0; i < formationManager.getAllyList ().Count; i++) {
			GameObject entity = formationManager.getAllyList () [i];
            if (entity == null)
                continue;
			Vector3 position = new Vector3 (entity.transform.position.x, 0, entity.transform.position.z);

			int selType = selectionType (clickDown, clickUp);
			bool inBox = false;

			if (selType == 0) {
				inBox = inBoxTopLeft (clickDown, clickUp, position);
			} else if (selType == 1) {
				inBox = inBoxBottomLeft (clickDown, clickUp, position);
			} else if (selType == 2) {
				inBox = inBoxTopRight (clickDown, clickUp, position);
			} else {
				inBox = inBoxBottomRight (clickDown, clickUp, position);
			}
			if (inBox) {
				selected.Add (entity);
			}
		}
	}

	public List<GameObject> getSelected(){
		return selected;
	}

	/********************************
	 *	PRIVATE UTILITY FUNCTIONS	*
	 ********************************/
	private int selectionType(Vector3 clickDown, Vector3 clickUp){
		int result = -1;
		if(clickDown.x < clickUp.x){
			if(clickDown.z > clickUp.z)
				result = 0;
			else 
				result = 1;
		} else{
			if (clickDown.z > clickUp.z)
				result = 2;
			else
				result = 3;
		}
		return result;
	}

	private bool inBoxTopLeft(Vector3 clickDown, Vector3 clickUp, Vector3 position){
		if (position.x < clickDown.x)
			return false;
		if (position.z > clickDown.z)
			return false;
		if (position.x > clickUp.x)
			return false;
		if (position.z < clickUp.z)
			return false;
		return true;
	}

	private bool inBoxBottomLeft(Vector3 clickDown, Vector3 clickUp, Vector3 position){
		if (position.x < clickDown.x)
			return false;
		if (position.z < clickDown.z)
			return false;
		if (position.x > clickUp.x)
			return false;
		if (position.z > clickUp.z)
			return false;
		return true;
	}

	private bool inBoxTopRight(Vector3 clickDown, Vector3 clickUp, Vector3 position){
		if (position.x > clickDown.x)
			return false;
		if (position.z > clickDown.z)
			return false;
		if (position.x < clickUp.x)
			return false;
		if (position.z < clickUp.z)
			return false;
		return true;
	}

	private bool inBoxBottomRight(Vector3 clickDown, Vector3 clickUp, Vector3 position){
		if (position.x > clickDown.x)
			return false;
		if (position.z < clickDown.z)
			return false;
		if (position.x < clickUp.x)
			return false;
		if (position.z > clickUp.z)
			return false;
		return true;
	}
}
