﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class InkRenderer : MonoBehaviour {
    LineRenderer ink;
    List<Vector3> positions = new List<Vector3>();

    private void OnEnable()
    {
        ink = this.gameObject.GetComponent<LineRenderer>();
        ink.sortingOrder = 3;
        updateInk();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        updateInk();
	}

    public void MoveToGlobal()
    {
        GameObject global = GameObject.Find("GlobalInk");
        //global.transform.Translate(new Vector3(1, 0, 0));
        ink.sortingOrder = 0;
        this.gameObject.transform.parent = global.transform;
    }

    private void updateInk()
    {
        positions.Add(this.gameObject.transform.position);
        ink.positionCount = positions.Count;
        ink.SetPositions(positions.ToArray());
    }
}
