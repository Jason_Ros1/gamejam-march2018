﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpawner : MonoBehaviour {

	[SerializeField] private GameObject gameManager;
	/**************/ private FormationManager formationManager;

	[Header("Entity Details")]
	[SerializeField] private AI.Team team;
	[SerializeField] private GameObject allyPrefab;
	[SerializeField] private GameObject hostilePrefab;

	[Header("Spawning Details")]
	[SerializeField] private float spawnRate;
	/**************/ private float timer;
	[SerializeField] private int spawnLimit;
	[SerializeField] private GameObject patrolGroup;
	/**************/ private PatrolPatterns patrolGroupScript;

	[Header("Spawning Location")]
	[SerializeField] private GameObject rallyPoint;
	[SerializeField] private float rallyPointXSize;
	[SerializeField] private float rallyPointYSize;


	// Use this for initialization
	void Start () {
		this.formationManager = gameManager.GetComponent<FormationManager> ();
		if(patrolGroup != null)
			patrolGroupScript = patrolGroup.GetComponent<PatrolPatterns>();
	} 
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (timer >= spawnRate) {
			timer -= spawnRate;
			spawnEntity();
		}		
	}

	private void spawnEntity(){
		Vector3 spawnLoc = new Vector3 (UnityEngine.Random.Range (-rallyPointXSize, +rallyPointXSize), 0, UnityEngine.Random.Range (-rallyPointYSize, rallyPointYSize)) + this.transform.position;

		if (this.team == AI.Team.Black && spawnLimit > 0) {
			GameObject newEntity = GameObject.Instantiate (allyPrefab, spawnLoc, Quaternion.identity) as GameObject;
			this.formationManager.addToEntityList (newEntity);
			spawnLimit--;
		} else if (this.team == AI.Team.White && spawnLimit > 0) {
			GameObject newEntity = GameObject.Instantiate (hostilePrefab, spawnLoc, Quaternion.identity) as GameObject;
			this.formationManager.addToEntityList (newEntity);
			spawnLimit--;
			if (patrolGroupScript != null) {
				patrolGroupScript.addToPatrol (newEntity);
			}
		}
	}
}
