﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explodeScript : MonoBehaviour {

    public Material PlayerTrail;
    public Material EnemyTrail;
    public float launchSpeed;
    public float timeDestroy = 1;
    Rigidbody rb;

    private void OnEnable()
    {
        rb = this.gameObject.GetComponent<Rigidbody>();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setExplosion(AI.Team a, Vector3 dir)
    {
        setLRmaterial(a);
        editVelocity(dir);
        StartCoroutine(waitAndDestroy());
    }

    IEnumerator waitAndDestroy()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(0.0f,timeDestroy));
        this.gameObject.GetComponent<InkRenderer>().MoveToGlobal();
        //Destroy(this.gameObject);
        rb.velocity = Vector3.zero;
    }

    public void setLRmaterial(AI.Team a)
    {
        LineRenderer lr = this.gameObject.GetComponent<LineRenderer>();
        if(a == AI.Team.Black)
        {
            lr.material = PlayerTrail;
        }
        else if(a == AI.Team.White)
        {
            lr.material = EnemyTrail;
        }
        else
        {
            Debug.Log("Wrong enumeration in LineRenderer Setting");
        }
        //lr.material = tmpMat;
    }

    public void editVelocity(Vector3 dir)
    {        
        Debug.Log("Direction: "+dir);
        rb.velocity = (dir * launchSpeed);
        Debug.Log(rb.velocity);
        //rb.velocity = velo;
    }
}
