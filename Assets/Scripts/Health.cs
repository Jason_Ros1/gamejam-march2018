﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
	
    public float maximumHealth = 100;
    [SerializeField] private float currentHealth;

	public GameObject gameManager;
	private GameStatManager gameStatManager;
    private FormationManager formationManager;

	private AI AIScript;
    //private MoveToGlobalParticle mtp;
    private InkRenderer inkr;

    public GameObject exp;

    AudioClipManager acm;
    

    void OnEnable() {
        gameManager = GameObject.Find("GameManager");
		AIScript = this.gameObject.GetComponent<AI> ();
        formationManager = gameManager.GetComponent<FormationManager>();
        currentHealth = maximumHealth;

        if (!AIScript.isPatrolLeader)
        {
            inkr = this.gameObject.transform.Find("InkRenderer").GetComponent<InkRenderer>();
            acm = this.gameObject.GetComponent<AudioClipManager>();
        }

    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}	

	/************************
	 *	PUBLIC FUNCTIONS	*
	 ************************/
     //TODO: Fix this problem
	public void takeDamage(float damage) {
		//gameStatManager.editHealthAggregate (-damage, this.AIScript.getTeam());
		currentHealth -= damage;
		if(currentHealth <= 0){
			onDeath();
		}
	}


    public float getHealth(){
		return this.currentHealth;
	}

	public void setManagers(GameObject gameManager){
		this.gameManager = gameManager;
		this.gameStatManager = this.gameManager.GetComponent<GameStatManager> ();
	}

	/********************************
	 *	PRIVATE UTILITY FUNCTIONS	*
	 ********************************/
	private void onDeath() {
		if (AIScript.isPatrolLeader)
			return;
		explode(AIScript.getTeam());
        inkr.MoveToGlobal();
        Debug.Log("PlayingDeath");
        acm.playDeath();
		GameObject.Destroy(this.gameObject);
	}

	private void explode(AI.Team a) {
        //spawn


        spawnExplosion(a,-15);
        spawnExplosion(a,-5);
        spawnExplosion(a,5);
        spawnExplosion(a,15);
        //-this.transform.forward;
    }

    private void spawnExplosion(AI.Team a, float num)
    {
        GameObject tmp = Instantiate(exp, this.transform.position, this.transform.rotation);
        explodeScript explosion = tmp.GetComponent<explodeScript>();

        Vector3 dir = Quaternion.Euler(0, num, 0) * (-this.transform.forward);

        explosion.setExplosion(a, dir);
    }
}
