﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInput : MonoBehaviour {
    public GameObject Player;
  //  MoveToLocation mtl;

	private FormationManager formationManager;
	private SelectionManager selectionManager;

	Vector3 leftButtonDownLoc;
	Vector3 leftButtonUpLoc;
	Vector3 rightButtonDownLoc;
	Vector3 rightButtonUpLoc;

	LineRenderer lineRenderer;


	// Use this for initialization
	void Start () {
 //      mtl = Player.GetComponent<MoveToLocation>();
		formationManager = this.gameObject.GetComponent<FormationManager> ();
		selectionManager = this.gameObject.GetComponent<SelectionManager> ();
		lineRenderer = this.gameObject.GetComponent<LineRenderer> ();
		lineRenderer.positionCount = 5;
		lineRenderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		Ray ray;
		RaycastHit hit;

        if (Input.GetMouseButtonDown(1))
        {
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
         
            if(Physics.Raycast(ray, out hit, 200.0f)) {
               	rightButtonDownLoc = hit.point;
                rightButtonDownLoc.y = 0;
            }
        }

		if (Input.GetMouseButtonUp (1)) {
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if(Physics.Raycast(ray, out hit, 200.0f)){
				ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				rightButtonUpLoc = hit.point;
				rightButtonUpLoc.y = 0;
                List<GameObject> temp = new List<GameObject>();
				formationManager.commandNewPosition (rightButtonDownLoc, rightButtonUpLoc, true, temp);
			}
		}		

		if (Input.GetMouseButtonDown(0)) {
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if(Physics.Raycast(ray, out hit, 200.0f)) {
				leftButtonDownLoc = hit.point;
				leftButtonDownLoc.y = 0;
			}

			lineRenderer.enabled = true;
		}

		if (Input.GetMouseButton (0)) {
			ray = Camera.main.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (ray, out hit, 200.0f)) {
				leftButtonUpLoc = hit.point;
				leftButtonUpLoc.y = 0;
			}
		}

		setLinePoints ();

		if (Input.GetMouseButtonUp (0)) {
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if(Physics.Raycast(ray, out hit, 200.0f)){
				ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				leftButtonUpLoc = hit.point;
				leftButtonUpLoc.y = 0;
				selectionManager.selectFromBox (leftButtonDownLoc, leftButtonUpLoc);
			}

			lineRenderer.enabled = false;
		}		
	}

	/********************************
	 *	PRIVATE UTILITY FUNCTIONS	*
	 ********************************/
	private void setLinePoints(){
		lineRenderer.SetPosition (0, new Vector3(this.leftButtonDownLoc.x, 5, this.leftButtonDownLoc.z));
		lineRenderer.SetPosition (1, new Vector3 (this.leftButtonDownLoc.x, 5, this.leftButtonUpLoc.z));
		lineRenderer.SetPosition (2, new Vector3(this.leftButtonUpLoc.x, 5, this.leftButtonUpLoc.z));
		lineRenderer.SetPosition (3, new Vector3 (this.leftButtonUpLoc.x, 5, this.leftButtonDownLoc.z));
		lineRenderer.SetPosition (4, new Vector3 (this.leftButtonDownLoc.x, 5, this.leftButtonDownLoc.z));
	}
}
