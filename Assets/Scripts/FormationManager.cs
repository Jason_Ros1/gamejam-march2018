﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FormationManager : MonoBehaviour {

	/*******************************************/
	public enum Formation{
		WEDGE,
		BOX
	};
	/*******************************************/

	[SerializeField] private float offset = 1.5f;
	[SerializeField] private int maxUnits = 300;

	[SerializeField] private SelectionManager selection;
	[SerializeField] private GameStatManager gameStatManager;

	private Formation formation;

	[SerializeField] private GameObject[] positions;
	public GameObject positionObject;

	[SerializeField] private List<GameObject> ally = new List<GameObject>();
	[SerializeField] private List<GameObject> hostile = new List<GameObject>();


	// Use this for initialization
	void Start () {
		selection = this.gameObject.GetComponent<SelectionManager> ();
		gameStatManager = this.gameObject.GetComponent<GameStatManager> ();
		//ally = new List<GameObject> ();
		//hostile = new List<GameObject> ();

		Vector3 spawnLoc = new Vector3 (0, 1f, 0);
		setFormation(Formation.WEDGE);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/*****************************
	 *		GETTER AND SETTERS	 *
	 *****************************/
	public void setFormation(Formation form){
		this.formation = form;
	}

	public List<GameObject> getAllyList(){
		return this.ally;
	}

	public List<GameObject> getHostileList(){
		return this.hostile;
	}

	/************************
	 *	PUBLIC FUNCTIONS	*
	 ************************/
	public void commandNewPosition (Vector3 position, Vector3 facing, bool player, List<GameObject> patrolUnits){
		killAllPositions ();
		resetAll ();
		if (player)
			calculateWedgeFormationPositions (selection.getSelected ().Count, position);
		else
			calculateWedgeFormationPositions (patrolUnits.Count, position);
		this.transform.position = position;
		this.transform.LookAt (facing);
		orderUnits (player, patrolUnits);
	}

	public void addToEntityList(GameObject entity){
		AI entityAI = entity.GetComponent<AI> ();
		Health entityHealth = entity.GetComponent<Health> ();

		if (entityAI.getTeam () == AI.Team.Black) {
			ally.Add (entity);
		} else if (entityAI.getTeam () == AI.Team.White) {
			hostile.Add (entity);
		} else {
			Debug.Log ("ERROR: Invalid Team value {only Black or White}");
		}
		gameStatManager.editHealthAggregate (entityHealth.getHealth (), entityAI.getTeam ());
		entityHealth.setManagers (this.gameObject);
	}

	/********************************
	 *	PRIVATE UTILITY FUNCTIONS	*
	 ********************************/
	private void calculateWedgeFormationPositions(int number, Vector3 position){
		if (number >= maxUnits) {
			Debug.Log ("ERROR: Too many units. Maximum is 300.");
			return;
		}

		positions = new GameObject[number];

		int row = 0;
		int numInRow = 0;
		int curInRow = 0;
		float posOffset = 0f;

		for (int i = 0; i < number; i++) {
			if (curInRow > numInRow) {
				numInRow++;
				row++;
				curInRow = 0;
				posOffset = row * (-offset);
			}

			if(curInRow != 0)
				posOffset += 2 * offset;

			//Debug.Log (curInRow + " Unit number: " + i + " with posOffset of " + posOffset + " with an offset value of " + offset );
			Vector3 localPos = new Vector3(posOffset, 0, row * (-offset));
			//Debug.Log(position);
	
			GameObject pos = GameObject.Instantiate (positionObject, localPos, Quaternion.identity) as GameObject;
			positions [i] = pos;
			positions [i].transform.parent = this.transform;
			curInRow++;
		}
	}

	private void resetAll(){
		this.transform.rotation = Quaternion.Euler (0, 0, 0);
		this.transform.position = new Vector3 (0, 0, 0);
	}

	private void orderUnits(bool player, List<GameObject> patrolUnits){
        List<GameObject> toBeCleaned = new List<GameObject>();
		for (int i = 0; i < positions.Length; i++) {
			Vector3 pos = positions [i].transform.position;
            if (player)
            {
                if (selection.getSelected()[i] == null)
                {
                    toBeCleaned.Add(patrolUnits[i]);
                } else
                    selection.getSelected()[i].GetComponent<AI>().targetSet(pos);
            }
            else
            {
                if (patrolUnits[i] == null)
                {
                    toBeCleaned.Add(patrolUnits[i]);
                }
                else
                    patrolUnits[i].GetComponent<AI>().targetSet(pos);
            }
		}
	}

	private void killAllPositions(){
		for(int i = 0; i < positions.Length; i++){
			if(positions[i] != null){
				GameObject.Destroy(positions[i]);
			}
		}
	}
}
