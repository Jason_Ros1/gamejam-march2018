﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AI))]
public class AI : MonoBehaviour {
    public NavMeshAgent agent;
    Agression agr;
    Agression comb;
    public float damageDeal = 5.0f;

	public bool isPatrolLeader;
    private bool isOnPatrol;

    public enum Team { Black, White }
    [SerializeField] private Team _team;
    //[SerializeField] private float aggroRadius = 2.5f;
    public enum State { Moving, Stopping }
    [SerializeField] private bool isAttacking = false;
    [SerializeField] private State _state = State.Stopping;

    [SerializeField] private float StopDistance = 0.2f;

    private Vector3 currentTarget = Vector3.zero;

    [SerializeField] private bool canAttack = true;
    [SerializeField] private float attackCooldown = 2.0f;
    AudioClipManager acm;

    public void OnEnable()
    {
        agent = this.GetComponent<NavMeshAgent>();
        //agent.enabled = false;
		if (!isPatrolLeader) {
			agr = this.transform.Find ("AggroRange").GetComponent<Agression> ();
			comb = this.transform.Find ("CombatRange").GetComponent<Agression> ();
            acm = this.gameObject.GetComponent<AudioClipManager>();
            //Debug.LogWarning(acm);

        }
        _state = State.Stopping;
        isOnPatrol = false;
    }



    public Team getTeam()
    {
        return _team;
    }

    public void setTeam(Team t) {  _team = t;   }
    public void setState(State i)   {  _state = i;   }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if(_state == State.Moving) { checkStop(); }

	}

    private void FixedUpdate()
    {
		if (!isPatrolLeader){
	        checkNeighbours();
    	    checkAttack();
		}
	}

    private void checkAttack()
    {
        if (canAttack)
        {
            if(comb.gO.Count >= 1)
            {
                GameObject enemyTarget = comb.getClosest();
                if(enemyTarget == null)
                {
                    Debug.Log("LINE 81: Enemy Target is NULL");
                    return;
                }
                isAttacking = true;
                enemyTarget.GetComponent<Health>().takeDamage(damageDeal);
                //Debug.Log(acm);
                acm.playSword();
                canAttack = false;
                StartCoroutine(resetAttack());
            }
            if(comb.gO.Count <= 0)
            {
                isAttacking = false;
            }
        }
    }

    IEnumerator resetAttack()
    {
        yield return new WaitForSeconds(attackCooldown);
        canAttack = true;
    }

    private void checkNeighbours()
    {
        if(agr.gO.Count >= 1)
        {
            //Debug.Log("Found Neighbours");
            //Debug.Log(agr.getClosest());
            this.isOnPatrol = false;
            if (agr.getClosest() == null)
            {
                agr.removeFromList(agr.getClosest());
                return;
            }
            if (currentTarget != agr.getClosest().transform.position)
            {
                if(_state == State.Stopping)
                {
                    //Debug.Log("Swapping Targets");
                    //currentTarget = agr.getClosest().transform.position;
                    targetSet(agr.getClosest().transform.position);
                }
            }
        }
        if(agr.gO.Count <= 0)
        {
            this.isOnPatrol = true;
            if (this.gameObject.tag == "White" && !this.isOnPatrol)
            {
                targetSet(this.gameObject.transform.position);
            }
        }
    }

    private void checkStop()
    {
        float disMag = (agent.destination - this.transform.position).magnitude;
        if (disMag < StopDistance)
        {
            agent.enabled = false;
            setState(State.Stopping);

        }

    }

    public void targetSet(Vector3 t)
    {
        //Debug.Log("Targetting");
        agent.enabled = true;
        agent.SetDestination(t);
        setState(State.Moving);
        currentTarget = t;
    }

	public bool isAttackingUnit(){
		return this.isAttacking;
	}

	public State getMoveState(){
		return this._state;
	}

    public void setOnPatrol(bool patrol)
    {
        this.isOnPatrol = patrol;
    }
}
