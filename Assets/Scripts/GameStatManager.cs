﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatManager : MonoBehaviour {

	[SerializeField] private float allyHealthAggregate;
	[SerializeField] private float hostileHealthAggregate;

	[SerializeField] private float battleStrPercentage;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		UpdateBattlePercentage ();
	}

	/************************
	 *	PUBLIC FUNCTIONS	*
	 ************************/
	public void editHealthAggregate(float amount, AI.Team team){
		if (team == AI.Team.Black)
			allyHealthAggregate += amount;
		else if (team == AI.Team.White)
			hostileHealthAggregate += amount;
		else
			Debug.Log ("ERROR: Invalid Team (Team.Black or Team.White only)");
	}

	public float getBattleStrPercentage(){
		return this.battleStrPercentage;
	}

	/********************************
	 *	PRIVATE UTILITY FUNCTIONS	*
	 ********************************/
	private void UpdateBattlePercentage(){
		float result;
		if ((allyHealthAggregate + hostileHealthAggregate) == 0)
			result = 1;
		else
			result = allyHealthAggregate / (allyHealthAggregate + hostileHealthAggregate);
		this.battleStrPercentage = result;
	}
}
