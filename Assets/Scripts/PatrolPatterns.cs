﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolPatterns : MonoBehaviour {

	public GameObject gameManager;
	private FormationManager formationManager;
	private AI AIScript;
	[SerializeField] private GameObject[] patrolWaypoints;
	[SerializeField] private List<GameObject> patrolUnits;
	/**************/ private List<AI> patrolUnitScript;

	private int currentWaypoint;

	/*****/
	private float timer;
	/*****/
	

	// Use this for initialization
	void Start () {
		formationManager = gameManager.GetComponent<FormationManager> ();
		AIScript = this.gameObject.GetComponent<AI> ();
		patrolUnitScript = new List<AI> ();
		currentWaypoint = 2;
		for (int i = 0; i < patrolUnits.Count; i++) {
			patrolUnitScript.Add (patrolUnits [i].GetComponent<AI> ());
		}
        this.AIScript.targetSet(this.patrolWaypoints[currentWaypoint].transform.position);
        assignRTF();
    }
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if (timer >= 1) {
			//Debug.Log ("LOGGING SECOND");
			timer -= 1;
		}
        if (AIScript.getMoveState() == AI.State.Stopping)
        {
            currentWaypoint++;
            if (currentWaypoint >= patrolWaypoints.Length)
                currentWaypoint = 0;
			Debug.Log("setting . . . " + currentWaypoint);

            this.AIScript.targetSet(this.patrolWaypoints[currentWaypoint].transform.position);  // The Patrol Pattern Object
            assignRTF();
        }
    }


	private void assignRTF(){
        //Debug.Log ("Assigning Return To Formation to " + patrolUnits.Count + " units");
		for (int i = 0; i < patrolUnits.Count; i++) {
			if (patrolUnitScript[i] == null)
				continue;
			if (!patrolUnitScript [i].isAttackingUnit() && patrolUnitScript [i].getMoveState() == AI.State.Stopping) {
                patrolUnitScript[i].setOnPatrol(true);
				formationManager.commandNewPosition (patrolWaypoints[currentWaypoint].transform.position, this.gameObject.transform.rotation.eulerAngles, false, patrolUnits);		
			}
		}

	}


	public int getNumOfUnits(){
		return this.patrolUnits.Count;
	}

	public void addToPatrol(GameObject unit){
		this.patrolUnits.Add (unit);
		this.patrolUnitScript.Add (unit.GetComponent<AI> ());
	}
}
