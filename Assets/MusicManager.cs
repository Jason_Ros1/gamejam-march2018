﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    AudioSource musicSource;
    [SerializeField] private List<AudioClip> musicList;
    [SerializeField] private int clip;

	// Use this for initialization
	void Start () {
        musicSource = this.gameObject.GetComponent<AudioSource>();
        clip = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if(musicSource.isPlaying == false)
        {
            Debug.Log("PLAYING");
            if(clip == 0)
            {
                playMenu();
            }
            else
            {
                playGameMusic();
            }
        }
	}

    public void playMenu()
    {
        if (musicSource.clip != null)
        {
            Debug.Log("Applying music 0");
            musicSource.clip = musicList[0];
            musicSource.loop = true;
            musicSource.Play();
        }
    }

    public void playGameMusic()
    {
        if (musicSource.clip != null)
        {
            Debug.Log("Applying music 1");
            musicSource.clip = musicList[1];
            musicSource.loop = true;
            musicSource.Play();
        }
        clip = 1;
    }
}
